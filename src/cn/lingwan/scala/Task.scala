package cn.lingwan.scala

import scala.actors.{Actor, Future}
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

case class SubmitTask(fileName:String)
case class ResultTask(result:Map[String,Int])
class Task extends Actor{
  override def act(): Unit = {
    loop{
      react{
        case SubmitTask(fileName) => {
          val data: String = Source.fromFile(fileName).mkString
          val lines: Array[String] = data.split("\r\n")
          val words: Array[String] = lines.flatMap(x=>x.split(" "))
          val wordAndOne: Array[(String, Int)] = words.map(word=>(word,1))
          val groupByWord: Map[String, Array[(String, Int)]] = wordAndOne.groupBy(x=>x._1)
          val result: Map[String, Int] = groupByWord.mapValues(x=>x.length)

          sender ! ResultTask(result)
        }
      }
    }
  }
}

object WordCount{
  def main(args: Array[String]): Unit = {
    val futureSet = new mutable.HashSet[Future[Any]]()
    val resultList = new ListBuffer[ResultTask]

    val files=Array("E:\\test\\aa.txt","E:\\test\\bb.txt","E:\\test\\cc.txt")
    for(f <- files){
      val task = new Task
      task.start()

      val reply: Future[Any] = task !! SubmitTask(f)

      futureSet+=reply
    }

    //iterate
    while (futureSet.size>0){
      val completedFutureSet: mutable.HashSet[Future[Any]] = futureSet.filter(x=>x.isSet)
      
      for (c <- completedFutureSet){
        val apply: Any = c.apply()
        resultList+=apply.asInstanceOf[ResultTask]
        futureSet-=c
      }
    }

    println(resultList.map(x=>x.result).flatten.groupBy(x=>x._1).mapValues(x=>x.foldLeft(0)((x,y)=>x+y._2)))

  }

}
